import nltk
import codecs
from nltk import FreqDist
from nltk.corpus import gutenberg
import json
from nltk.tag.stanford import POSTagger
import csv
import datetime


f = open("train_tag.txt",'r')
line = f.read()
line = line.replace("\n"," ")
line = line.replace("._. ","._.")
lines = line.split("._.")


fi = open('its.train1', 'w')
fy = open('your.train1', 'w')
ft = open('their.train1', 'w')
fl = open('lose.train1', 'w')
fto = open('too.train1', 'w')


for i in range(len(lines)-1):
    #print(lines[i])
    ll = lines[i].lstrip()
    ll = ll.strip('\n')
    word = ll.split(" ")
    ss = 's_s'+" "+'s_s'+" "
    for j in range(len(word)):
        ss += word[j]+" "
    ss += 'e_e'+" "+'e_e'+" "+'e_e'
    wd = ss.split()

    for t in range(2,len(wd)-3):
        temp = wd[t].split("_") 
        #print(temp)                  
        prev1 = wd[t-1].split("_")
        prev2 = wd[t-2].split("_")
        next1 = wd[t+1].split("_")
        next2 = wd[t+2].split("_")
        next3 = wd[t+3].split("_")

        if ((temp[0].lower() == 'its') or ((temp[0].lower() == "it") and (next1[0] == "'s"))):

            if (temp[0].lower() =='its'):
                ss = "its"+" "+"prev2:"+prev2[0]+" "+'prev2t:'+prev2[0]+" "+"prev1:"+prev1[0]+" "+"prev1t:" + prev1[1]+" "+"next1:"+next1[0]+" "+"next2:"+next2[0]+"\n"
                fi.write(ss)
            elif ((temp[0].lower() =='it') and (next1[0] == "'s")):
                ss = "itis"+" "+"prev2:"+prev2[0]+" "+'prev2t:'+prev2[0]+" "+"prev1:"+prev1[0]+" "+"prev1t:" + prev1[1]+" "+"next1:"+next2[0]+" "+"next2:"+next3[0]+"\n"
                fi.write(ss)

        elif ((temp[0].lower() == 'your') or ((temp[0].lower() == "you") and (next1[0] == "'re"))):
            if (temp[0].lower() =='your'):
                ss = "your"+" "+"prev2:"+prev2[0]+" "+'prev2t:'+prev2[0]+" "+"prev1:"+prev1[0]+" "+"prev1t:" + prev1[1]+" "+"next1:"+next1[0]+" "+"next2:"+next2[0]+"\n"
                fy.write(ss)
            elif ((temp[0].lower() =='you') and (next1[0] == "'re")):
                ss = "youre"+" "+"prev2:"+prev2[0]+" "+'prev2t:'+prev2[0]+" "+"prev1:"+prev1[0]+" "+"prev1t:" + prev1[1]+" "+"next1:"+next2[0]+" "+"next2:"+next3[0]+"\n"
                fy.write(ss)

        elif ((temp[0].lower() == 'their') or ((temp[0].lower() == "they") and (next1[0] == "'re"))):
            if (temp[0].lower() =='their'):
                ss = "their"+" "+"prev2:"+prev2[0]+" "+'prev2t:'+prev2[0]+" "+"prev1:"+prev1[0]+" "+"prev1t:" + prev1[1]+" "+"next1:"+next1[0]+" "+"next2:"+next2[0]+"\n"
                ft.write(ss)
            elif ((temp[0].lower() =='they') and (next1[0] == "'re")):
                ss = "theyre"+" "+"prev2:"+prev2[0]+" "+'prev2t:'+prev2[0]+" "+"prev1:"+prev1[0]+" "+"prev1t:" + prev1[1]+" "+"next1:"+next2[0]+" "+"next2:"+next3[0]+"\n"
                ft.write(ss)

        elif ((temp[0].lower() == 'lose') or (temp[0].lower() == "loose")):
            ss = temp[0].lower()+" "+"prev2:"+prev2[0]+" "+'prev2t:'+prev2[0]+" "+"prev1:"+prev1[0]+" "+"prev1t:" + prev1[1]+" "+"next1:"+next1[0]+" "+"next2:"+next2[0]+"\n"
            fl.write(ss)
                                    
        elif ((temp[0].lower() == 'to') or (temp[0].lower() == "too")):
            ss = temp[0].lower()+" "+"prev2:"+prev2[0]+" "+'prev2t:'+prev2[0]+" "+"prev1:"+prev1[0]+" "+"prev1t:" + prev1[1]+" "+"next1:"+next1[0]+" "+"next2:"+next2[0]+"\n"
            fto.write(ss)
                                    
                                 


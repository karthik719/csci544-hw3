import nltk
import codecs
from nltk import FreqDist
from nltk.corpus import gutenberg
import json
from nltk.tag.stanford import POSTagger
import csv
import datetime

ff = open('testetag', 'w')
f = open('hw3.test.err.txt', "r")

for line in f:
    sa = ''
    le = len(line)
    if le > 1:      
        tokens = line.split()
        poss = nltk.pos_tag(tokens)
        l = len(poss)
        for i in range(l):
            sa += poss[i][0]+"/"+poss[i][1]+" "
        sa = sa.rstrip()
        ff.write(sa)
        ff.write('\n') 
    else:
        ff.write(line)




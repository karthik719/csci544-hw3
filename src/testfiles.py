import nltk
import sys
import re
from string import punctuation

ff = open("testetag",'r')

fi = open('its.test', 'w')
fy = open('your.test', 'w')
ft = open('their.test', 'w')
fl = open('lose.test', 'w')
fto = open('too.test', 'w') 

for line in ff:
    word = line.split()
    if len(word) > 1:
        tok = ['s/s','s/s']
        for j in range(len(word)):
            tok.append(word[j])
        for p in range(3):
            tok.append('e/e')

        for k in range(2,len(tok)-3):

            temp = tok[k].split("/")[0]                  
            prev1 = "prev1:"+tok[k-1].split("/")[0].strip(punctuation)
            prevt = "prev1t:"+tok[k-1].split("/")[1]
            prevt2 = "prev2t:"+tok[k-2].split("/")[1]
            prev2 = "prev2:"+tok[k-2].split("/")[0].strip(punctuation)
            next1 = "next1:"+tok[k+1].split("/")[0].strip(punctuation)
            next2 = "next2:"+tok[k+2].split("/")[0].strip(punctuation)

            if ((temp.strip(punctuation).lower() == 'its') or (temp.strip(punctuation).lower() == "it's")):
                if (temp.strip(punctuation).lower() == 'its'):
                    st = "its"+" "+prev2+" "+prevt2+" "+prev1+" "+prevt+" "+next1+" "+next2+'\n'
                    fi.write(st)

                elif (temp.strip(punctuation).lower() == "it's"):
                    st = "itis"+" "+prev2+" "+prevt2+" "+prev1+" "+prevt+" "+next1+" "+next2+'\n'
                    fi.write(st)

            elif ((temp.strip(punctuation).lower() == 'your') or (temp.strip(punctuation).lower() == "you're")):
                if (temp.strip(punctuation).lower() =='your'):
                    st = "your"+" "+prev2+" "+prevt2+" "+prev1+" "+prevt+" "+next1+" "+next2+'\n'
                    fy.write(st)

                elif (temp.strip(punctuation).lower() == "you're"):
                    st = "youre"+" "+prev2+" "+prevt2+" "+prev1+" "+prevt+" "+next1+" "+next2+'\n'
                    fy.write(st)

            elif ((temp.strip(punctuation).lower() == 'their') or (temp.strip(punctuation).lower() == "they're")):
                if (temp.strip(punctuation).lower() =='their'):
                    st = "their"+" "+prev2+" "+prevt2+" "+prev1+" "+prevt+" "+next1+" "+next2+'\n'
                    ft.write(st)

                elif (temp.strip(punctuation).lower() == "they're"):
                    st = "theyre"+" "+prev2+" "+prevt2+" "+prev1+" "+prevt+" "+next1+" "+next2+'\n'
                    ft.write(st)

            elif ((temp.strip(punctuation).lower() == 'lose') or (temp.strip(punctuation).lower() == "loose")):
                st = temp.strip(punctuation).lower()+" "+prev2+" "+prevt2+" "+prev1+" "+prevt+" "+next1+" "+next2+'\n'
                fl.write(st)                 
                   
            elif (temp.strip(punctuation).lower() == 'to') or (temp.strip(punctuation).lower() == "too"):
                st = temp.strip(punctuation).lower()+" "+prev2+" "+prevt2+" "+prev1+" "+prevt+" "+next1+" "+next2+'\n'
                fto.write(st)                        
                                 

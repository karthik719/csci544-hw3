import sys
import string
import nltk
from string import punctuation

f = open('hw3.test.err.txt', "r")
f1 = open('its.output' , 'r')
f2 = open('their.output' , 'r')
f3 = open('your.output' , 'r')
f4 = open('lose.output' , 'r')
f5 = open('too.output' , 'r')
f6 = open('hw3.dev.cor.txt' , 'w')

rep_it = []
rep_their = []
rep_your = []
rep_lose = []
rep_too = []

for line in f1:
    wd = line.split()
    rep_it.append(wd[0])

for line in f2:
    wd = line.split()
    rep_their.append(wd[0])

for line in f3:
    wd = line.split()
    rep_your.append(wd[0])

for line in f4:
    wd = line.split()
    rep_lose.append(wd[0])

for line in f5:
    wd = line.split()
    rep_too.append(wd[0])

print(len(rep_it))
print(len(rep_their))
print(len(rep_your))
print(len(rep_lose))
print(len(rep_too))

j = 0
n = 0 
m = 0 
for line in f:
    word = line.split()
    ss = ''
    for i in range(len(word)):
        #print(word[i])
        if (word[i].strip(punctuation).lower() == 'its'):
            if rep_it[j] == 'its':
                n += 1
                ss += 'its'+" "
            elif rep_it[j] == 'itis':
                m += 1
                ss += "it's"+" "
            j += 1
        elif (word[i].strip(punctuation).lower() == "it's"):
            if rep_it[j] == 'its':
                n += 1
                ss += 'its'+" "
            elif rep_it[j] == 'itis':
                m += 1
                ss += "it's"+" "
            j += 1
        else:
             ss += word[i]+" "
    f6.write(ss)
    f6.write('\n')
print(n)
print(m)
f.close()
f6.close()

f7 = open('hw3.dev.cor.txt1', "w")
f = open('hw3.dev.cor.txt', "r")

j = 0
n = 0 
m = 0 
for line in f:
    word = line.split()
    ss = ''
    for i in range(len(word)):
        #print(word[i])
        if (word[i].strip(punctuation).lower() == 'their'):
            if rep_their[j] == 'their':
                n += 1
                ss += 'their'+" "
            elif rep_their[j] == 'theyre':
                m += 1
                ss += "they're"+" "
            j += 1
        elif (word[i].strip(punctuation).lower() == "they're"):
            if rep_their[j] == 'their':
                n += 1
                ss += 'their'+" "
            elif rep_their[j] == 'theyre':
                m += 1
                ss += "they're"+" "
            j += 1
        else:
             ss += word[i]+" "
    f7.write(ss)
    f7.write('\n')
print(n)
print(m)

f.close()
f7.close()

f8 = open('hw3.dev.cor.txt2', "w")
f = open('hw3.dev.cor.txt1', "r")


j = 0
n = 0 
m = 0 
for line in f:
    word = line.split()
    ss = ''
    for i in range(len(word)):
        #print(word[i])
        if (word[i].strip(punctuation).lower() == 'your'):
            if rep_your[j] == 'your':
                n += 1
                ss += 'your'+" "
            elif rep_your[j] == 'youre':
                m += 1
                ss += "you're"+" "
            j += 1
        elif (word[i].strip(punctuation).lower() == "you're"):
            if rep_your[j] == 'your':
                n += 1
                ss += 'your'+" "
            elif rep_your[j] == 'youre':
                m += 1
                ss += "you're"+" "
            j += 1
        else:
             ss += word[i]+" "
    f8.write(ss)
    f8.write('\n')
print(n)
print(m)

f.close()
f8.close()

f9 = open('hw3.dev.cor.txt3', "w")
f = open('hw3.dev.cor.txt2', "r")


j = 0
n = 0 
m = 0 
for line in f:
    word = line.split()
    ss = ''
    for i in range(len(word)):
        #print(word[i])
        if (word[i].strip(punctuation).lower() == 'lose'):
            if rep_lose[j] == 'lose':
                n += 1
                ss += 'lose'+" "
            elif rep_lose[j] == 'loose':
                m += 1
                ss += "loose"+" "
            j += 1
        elif (word[i].strip(punctuation).lower() == "loose"):
            if rep_lose[j] == 'lose':
                n += 1
                ss += 'lose'+" "
            elif rep_lose[j] == 'loose':
                m += 1
                ss += "loose"+" "
            j += 1
        else:
             ss += word[i]+" "
    f9.write(ss)
    f9.write('\n')
print(n)
print(m)


f.close()
f9.close()

f10 = open('hw3.dev.cor.txt4', "w")
f = open('hw3.dev.cor.txt3', "r")


j = 0
n = 0 
m = 0 
for line in f:
    word = line.split()
    ss = ''
    for i in range(len(word)):
        #print(word[i])
        if (word[i].strip(punctuation).lower() == 'to'):
            if rep_too[j] == 'to':
                n += 1
                ss += 'to'+" "
            elif rep_too[j] == 'too':
                m += 1
                ss += "too"+" "
            j += 1
        elif (word[i].strip(punctuation).lower() == "too"):
            if rep_too[j] == 'to':
                n += 1
                ss += 'to'+" "
            elif rep_too[j] == 'too':
                m += 1
                ss += "too"+" "
            j += 1
        else:
             ss += word[i]+" "
    f10.write(ss)
    f10.write('\n')
print(n)
print(m)

f.close()
f10.close()

